const daysDiv = document.getElementById('days');
const hoursDiv = document.getElementById('hours');
const minutesDiv = document.getElementById('minutes');
const secondsDiv = document.getElementById('seconds');
let releaseDisplayed = false;


daysDiv.innerText = Math.floor(initDistant / (day)).toString();
hoursDiv.innerText = Math.floor((initDistant % (day)) / (hour)).toString();
minutesDiv.innerText = Math.floor((initDistant % (hour)) / (minute)).toString();
secondsDiv.innerText = Math.floor((initDistant % (minute)) / second).toString();

function released() {
        if(!releaseDisplayed) {
                let domEle = document.getElementById("days");
                domEle.parentElement.parentElement.style.display = "none";
                domEle.parentElement.parentElement.previousElementSibling.style.display = "none";
                let releaseRow = document.createElement("div");
                releaseRow.className = "row";
                let releaseColoum = document.createElement("div");
                releaseColoum.className = "col align-self-center";
                let releaseContent = document.createElement("div");
                releaseContent.id = "released";
                let releaseContentText = document.createTextNode("RELEASED");
                releaseRow.appendChild(releaseColoum);
                releaseColoum.appendChild(releaseContent);
                releaseContent.appendChild(releaseContentText);
                domEle.parentElement.parentElement.parentElement.appendChild(releaseRow);
                releaseDisplayed = true;
        }
}

let countDown = countDownTarget,
    x = setInterval(function() {

            let now = new Date().getTime(),
                distance = countDown - now;

            let days = Math.floor(distance / (day));
            let hours = Math.floor((distance % (day)) / (hour));

            let minutes = Math.floor((distance % (hour)) / (minute));
            let seconds = Math.floor((distance % (minute)) / (second));

            let domDays = document.getElementById('days');
            let domHours = document.getElementById('hours');
            let domMinutes = document.getElementById('minutes');
            let domSeconds = document.getElementById('seconds');
            if(days < 0 || hours < 0 || minutes < 0 || seconds < 0) {
                    released();
            } else {
                    domDays.innerText = days.toString();
                    domHours.innerText = hours.toString();
                    domMinutes.innerText = minutes.toString();
                    domSeconds.innerText = seconds.toString();
            }


    }, second);
